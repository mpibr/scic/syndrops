#!/bin/bash

#SBATCH --nodes=1
#SBATCH --partition=cuttlefish
#SBATCH --time=100:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --job-name=StarSolo
#SBATCH --error=error_%j.txt
#SBATCH --output=output_%j.txt

# --- configure environment --- #
path_bclconvert="/gpfs/scic/software/biotools/bcl-convert"
path_fastp="/gpfs/scic/software/biotools/fastp"
path_star="/gpfs/scic/software/biotools/STAR-2.7.11a/bin/Linux_x86_64_static"
path_htslib="/gpfs/scic/software/biotools/htslib/bin"
path_reference="/gpfs/scic/data/genomes/Mouse/mm39/stardb"
path_project="/gpfs/scic/data/projects/syndrop"
file_config="${path_project}/config/config.csv"
adapter_nextera="CTGTCTCTTATACACATCTCCGAGCCCACGAGAC"
adapter_file="${path_project}/barcodes/adapter.fasta"

# provide configuration
if [ ! -f "$file_config" ]; then
    echo "Config file $file_config does not exist."
    exit 1
fi

while IFS=',' read -r id path_run samplesheet barcodes; do
    
    
    # current run
    echo "# ------------------------- #"
    echo "Run: $id"
    echo "Path: $path_run"
    echo "Samplesheet: $samplesheet"
    echo "Barcodes: $barcodes"
    echo "# ------------------------- #"
    
    path_job="${path_project}/${id}"
    mkdir -p "$path_job"
    
    
    
    # bcl-convert
    path_fastq="${path_job}/fastq"
    if [ ! -d "$path_fastq" ]; then
        echo "# --- demultiplexing --- #"
        mkdir -p "$path_fastq"
        "${path_bclconvert}/bcl-convert" \
        --force \
        --bcl-input-directory "$path_run" \
        --output-directory "$path_fastq" \
        --sample-sheet "$samplesheet" \
        --num-unknown-barcodes-reported 100 \
        --no-lane-splitting
    fi
    

    # clean adapters fastp
    path_fastq_clean="${path_fastq}/clean"
    if [ ! -d "$path_fastq_clean" ]; then
        echo "# --- adapter trimming --- #"
        mkdir -p "$path_fastq_clean"
        for file_fastq_r1 in ${path_fastq}/${id}_*R1_001.fastq.gz; do
            file_fastq_r2=$(echo "$file_fastq_r1" | perl -p -e "s/\_R1\_/\_R2\_/g")
            file_label_out=$(basename "$file_fastq_r1" _R1_001.fastq.gz)
            if [[ -f "$file_fastq_r1" && -f "$file_fastq_r2" ]]; then
                echo "$file_label_out"
                "${path_fastp}/fastp" \
                --in1=$file_fastq_r1 \
                --in2=$file_fastq_r2 \
                --out1=${path_fastq_clean}/${file_label_out}_R1_001_clean.fastq.gz \
                --out2=${path_fastq_clean}/${file_label_out}_R2_001_clean.fastq.gz \
                --compression=4 \
                --adapter_sequence_r2="$adapter_nextera" \
                --adapter_fasta="$adapter_file" \
                --trim_poly_g \
                --trim_poly_x \
                --length_required 22 \
                --thread=16 \
                --json=${path_fastq_clean}/${file_label_out}_fastp.json \
                --html=${path_fastq_clean}/${file_label_out}_fastp.html \
                2> ${path_fastq_clean}/${file_label_out}_fastp.txt
            fi
        done
        
    fi

    # star-solo
    path_bams="${path_job}/bams"
    if [ ! -d "$path_bams" ]; then
        echo "# --- star-solo --- #"
        mkdir -p "$path_bams"
        for file_fastq_r1 in ${path_fastq_clean}/${id}_*R1_001_clean.fastq.gz; do
            file_fastq_r2=$(echo "$file_fastq_r1" | perl -p -e "s/\_R1\_/\_R2\_/g")
            file_label_out=$(basename "$file_fastq_r1" _R1_001_clean.fastq.gz)
            if [[ -f "$file_fastq_r1" && -f "$file_fastq_r2" ]]; then
                # star-solo
                "${path_star}/STAR" \
                --runThreadN 16 \
                --genomeDir "$path_reference" \
                --readFilesCommand zcat \
                --readFilesIn "$file_fastq_r2" "$file_fastq_r1" \
                --outSAMtype BAM SortedByCoordinate \
                --soloType CB_UMI_Complex \
                --soloCBposition 0_0_0_5 0_8_0_13 0_16_0_21 \
                --soloUMIposition 1_0_1_15 \
                --soloCBwhitelist "$barcodes" "$barcodes" "$barcodes" \
                --soloCBmatchWLtype 1MM \
                --soloUMIdedup NoDedup \
                --soloFeatures GeneFull_Ex50pAS \
                --soloMultiMappers Uniform \
                --soloStrand Reverse \
                --outFileNamePrefix "${path_bams}/${file_label_out}_"

                # clean result
                mv "${path_bams}/${file_label_out}_Aligned.sortedByCoord.out.bam" "${path_bams}/${file_label_out}.bam"
                mv "${path_bams}/${file_label_out}_Log.final.out" "${path_bams}/${file_label_out}_log.txt"
                mv "${path_bams}/${file_label_out}_SJ.out.tab" "${path_bams}/${file_label_out}_SJ.txt"
                mv "${path_bams}/${file_label_out}_Solo.out" "${path_bams}/${file_label_out}_solo"
                rm -f ${path_bams}/${file_label_out}_*.out

                # bam index
                "${path_htslib}/samtools" index "${path_bams}/${file_label_out}.bam"
                
            fi
        done
    fi
    
done < <(tail -n +2 "$file_config")

echo "# --- successfully done --- #"


            

