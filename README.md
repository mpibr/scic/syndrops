# syndrops

sequence analysis of synaptic droplets

#### packages
- [scanpy](https://github.com/scverse/scanpy)
- [tacco](https://github.com/simonwm/tacco)


#### prepare environment

```
% micromamba create -f environment.yaml -p ./venv
% micromamba activate -p ./venv
```

#### preprare remote jupyter server

```
$ jupyter lab --generate-config
Writing default config to: /gpfs/scic/personal/tushevg/.jupyter/jupyter_lab_config.py
$
```
