############################################################################################################################
# echo QualityFiltering #Deduplication and removal of reads with low complexity or containing homopolymers or adapters
# mkdir DeMultiplexed_Fastqs/
# mkdir Fastp_Files
# 
# for l in *R1.fastq.gz;
# 
# do
# Exp=$(echo "$l"|cut -d "_" -f1)
# echo $Exp"_R2.fastq.gz"
# 
# /gpfs/scic/software/biotools/fastp/fastp -y -x -3 -l 26 -D\
#   --adapter_fasta adapter.fasta \
#   -i ${l} -I $Exp"_R2.fastq.gz" \
#   -o Filtered_${l} -O "Filtered_"$Exp"_R2.fastq.gz"
# mv ${l} DeMultiplexed_Fastqs/
# mv $Exp"_"$number"_"$sample"_R2_001.fastq.gz" DeMultiplexed_Fastqs/
# done

############################################################################################################################
# echo FastqQC #this step calculates several quality metrics for the sequencing run
# 
# mkdir Filtered_Fastqs
# mkdir Filtered_Fastqs/FastQC
# 
# /gpfs/scic/software/biotools/FastQC/fastqc *.fastq.gz
# mv *fastqc* Filtered_Fastqs/FastQC/

##########################################################################################################################
# echo "Step3:RemoveBasesConnectingIndexes"
# #The barcode been read in Read1 has the following structure JJJJJJGAJJJJJJGTJJJJJJTTTT where J is a base within one of the,
# #indexes that makes up the barcode. Here we remove the connecting bases in positions 7,8,15,16, and the final 4 bases
# 
# Exp=$(echo "S315")
# 
# for l in *R1.fastq.gz;
# do
# 
# positions="7,8,15,16,23,24,25,26"
# zcat ${l} | awk -v positions="$positions" '
# BEGIN {
#   split(positions, pos_array, ",");
#   for (i in pos_array) {
#     pos_map[pos_array[i]] = 1;
#   }
# }
# {
#   if (NR % 4 == 2) {
#     new_seq = "";
#     for (i = 1; i <= length($0); ++i) {
#       if (!(i in pos_map)) {
#         new_seq = new_seq substr($0, i, 1);
#       }
#     }
#     print new_seq "TACG";
#   } else if (NR % 4 == 0) {
#     new_qual = "";
#     for (i = 1; i <= length($0); ++i) {
#       if (!(i in pos_map)) {
#         new_qual = new_qual substr($0, i, 1);
#       }
#     }
#     print new_qual "IIII";  # Adds 'I' quality score to the added bases. Adjust this as needed.
#   } else {
#     print;
#   }
# }' | gzip > Edited_${l}
# mv ${l} DeMultiplexed_Fastqs/
# done


############################################################################################################################
# echo STAR_Alignment #Align reads to the mouse genome and per-barcode read quantification
# mkdir Alignment_Logs/
# 
# for l in *R1.fastq.gz;
# 
# do
# Exp=$(echo "S315")
# echo "Filtered_"$Exp"_R2.fastq.gz"
# 
# /gpfs/scic/software/biotools/STAR-2.7.10a/build/STAR --genomeDir /gpfs/schu/data/RNA/Projects/Single_dendrite_RNA-seq/Genomes/Mouse/Spiked_Gencode_Stardb/ \
#   --readFilesIn "Filtered_"$Exp"_R2.fastq.gz" ${l} \
#   --runThreadN 8 \
#   --outStd Log \
#   --outSAMtype BAM SortedByCoordinate \
#   --readFilesCommand zcat \
#   --outFileNamePrefix $Exp"_" \
#   --outFilterScoreMinOverLread 0.66 \
#   --outFilterMatchNminOverLread 0.66 \
#   --soloType CB_UMI_Simple \
#   --soloCBstart 1 \
#   --soloCBlen 18 \
#   --soloUMIstart 19 \
#   --soloUMIlen 4 \
#   --soloCBmatchWLtype 1MM_multi_Nbase_pseudocounts \
#   --soloCBwhitelist SynDrops_D4.1_whitelist.txt \
#   --soloUMIdedup NoDedup \
#   --soloFeatures GeneFull \
#   --soloMultiMappers PropUnique \
#   --outSAMattributes CR CB \
#   --quantMode GeneCounts
# 
# done

#############################################################################################################################
#############################################################################################################################
#Generation, QC and quantification of 3 pseudo bulk files: 1) all gene-mapping reads regardless of barcoded read status, 
#2) all gene-mapping reads where the barcoded read does not match the whitelist, and 
#3) all gene-mapping reads where the barcoded read does match the whitelist

# echo Split BAMs
# 
# for l in *.bam;
# 
# do
# /gpfs/scic/software/biotools/htslib/bin/samtools split -d UB:Z:TACG ${l} #all barcoded reads contain the TACG sequence in the UMI slot
# done

###########################################################################################################################
echo RNASeqMetrics
mkdir QC_Files/

for m in *.bam;

do
java -jar /gpfs/scic/software/biotools/picard/build/libs/picard.jar CollectRnaSeqMetrics \
      I=${m}\
      O=${m}.RNA_Metrics\
      REF_FLAT=/gpfs/schu/data/RNA/Projects/Single_dendrite_RNA-seq/Genomes/Mouse/Spiked_gencode.refFlat \
      STRAND=NONE \
      CHART_OUTPUT=${m}.pdf\
      RRNA_FRAGMENT_PERCENTAGE=0.8\
      MINIMUM_LENGTH=500\
      RIBOSOMAL_INTERVALS=/gpfs/schu/data/RNA/Projects/Single_dendrite_RNA-seq/Genomes/Mouse/Gencode_Intervals/Mouse_Gencode.rRNA.intervals
done
mv *RNA_Metrics QC_Files/


#############################################################################################################################
# echo FeatureCounts #Assign reads to genes
# 
# /gpfs/scic/software/biotools/subread-2.0.3/bin/featureCounts -a /gpfs/schu/data/RNA/Projects/Single_dendrite_RNA-seq/Genomes/Mouse/Spiked_gencode.gtf \
#  -o PseudoBulk.counts  -T 8 *.bam








