# Project: SynapticDiversity
#
# Task: 
#    * compare synaptosome connectome
#
# Date: Oct 2023
# Author: Georgi Tushev
#         Scientific Computing and Data Visualisation Facility
#         Max Planck Institute For Brain Research
#
# Bugs report: georgi.tushev@brain.mpg.de

library(rstudioapi)
library(ggplot2)
library(igraph)


# --- set working directory --- #
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

# read marker genes
markers <- read.table("tableMarkers_reduced_01Jul2023.txt", header = T, row.names = 1, sep = "\t")

# read features
features <- read.table("features_meta.txt", header = F, sep = "\t")
features <- features$V1
species <- rep("mm39", length(features))
species[startsWith(features, "rn7")] <- "rn7"
features <- stringr::str_replace_all(features, "mm39_", "")
features <- stringr::str_replace_all(features, "rn7__", "")

# get reads summary table
conn <- read.table("10xS185/conn.mtx.gz", header = F)
colnames(conn) <- c("i", "j", "corr")
conn$corr <- log2(conn$corr)


n <- max(max(conn$i),max(conn$j))
n <- 50559
idx <- conn$i <= n & conn$j <= n & conn$corr > 4
tmp <- conn[idx,]

g <- graph_from_data_frame(data.frame(from = tmp$i,
                                      to = tmp$j))
E(g)$weight <- tmp$corr
#g <- simplify(g, edge.attr.comb="sum")



# # Calculate weighted degree for each vertex
# weighted_degrees <- sapply(V(g), function(vertex) {
#   sum(E(g)[from(vertex) | to(vertex)]$weight)
# })
# 
# 
# vertex_order <- order(weighted_degrees, decreasing = TRUE)
# g <- permute(g, vertex_order)

#weighted_degrees <- V(g)$weight
#vertex_order <- order(weighted_degrees, decreasing = TRUE)
#g <- permute(g, vertex_order)

idx_delete <- degree(g) <= 2 | degree(g) > 50
g <- delete.vertices(g, V(g)[idx_delete])
eb <- cluster_walktrap(g)

plot(eb, g, 
     layout = layout.fruchterman.reingold, 
     vertex.size = 1, 
     vertex.label = NA, 
     vertex.color = 'black',
     edge.arrow.mode = "none",
     edge.color = rgb(0.8,0.8,0.8, alpha = 0.1))

