#!/usr/bin/perl

use warnings;
use strict;
use File::Spec;

sub min($$) {return $_[0] <= $_[1] ? $_[0] : $_[1]};
sub max($$) {return $_[0] <= $_[1] ? $_[1] : $_[0]};


main:{
    my $path_run = shift;

    my $file_barcodes = File::Spec->catfile($path_run, "barcodes.tsv.gz");
    my $file_features = File::Spec->catfile($path_run, "features.tsv.gz");
    my $file_matrix = File::Spec->catfile($path_run, "matrix.mtx.gz");
    
    # accumulate counts
    my %table = ();
    open (my $fh, "gzcat $file_matrix | tail -n +4|") or die $!;
    while(<$fh>) {
        chomp($_);
        my ($gid, $bid, $count) = split(" ", $_, 3);
        push(@{$table{$bid}}, [$gid, $count]);
    }
    close($fh);

    # create connections
    my %conn = ();
    foreach my $bid (keys %table) {
        my $n = scalar(@{$table{$bid}});
        next if($n <= 1);
        foreach my  $pack_a (@{$table{$bid}}) {
            my $gid_a = $pack_a->[0];
            my $val_a = $pack_a->[1];
            foreach my $pack_b (@{$table{$bid}}) {
                my $gid_b = $pack_b->[0];
                my $val_b = $pack_b->[1];
                next if ($gid_a == $gid_b);
                my $i = min($gid_a, $gid_b);
                my $j = max($gid_a, $gid_b);
                $conn{$i}{$j} += $val_a + $val_b;
            }
        }
    }

    # print connections
    foreach my $i (sort { $a <=> $b } keys %conn) {
        foreach my $j (sort { $a <=> $b } keys %{$conn{$i}}) {
            print $i,"\t",$j,"\t",$conn{$i}{$j},"\n";
        }
    }
    
}
