#include <iostream>
#include <unordered_map>
#include <zlib.h>
#include <htslib/hts.h>
#include <htslib/kseq.h>
//#include "whitelist.h"
#include "solowhitelist.h"


KSEQ_INIT(gzFile, gzread)

int main(const int argc, const char *argv[])
{
    const std::string file_barcodes = std::string(argv[2]);
    const std::string file_fastq = std::string(argv[1]);
    //const std::string barcodeX = "AAACCGCG";
    gzFile fp;
	kseq_t *kseq;
	int r, n = 0;
    int p = 0;
    std::unordered_map<std::string, int> counts;
    std::unordered_map<std::string, int> states;
    

    // create whitelist
    solowhitelist wl;
    if (wl.load(file_barcodes) != 0) {
        return -1;
    }
    std::cerr <<"syndrops:: whitelist loaded " << wl.size() << " barcodes." << std::endl;

    // parse fastq file
    fp = gzopen(file_fastq.c_str(), "r");
	kseq = kseq_init(fp);
	while ((r = kseq_read(kseq)) >= 0) {
        n++;
        std::string seq(kseq->seq.s);
        std::string b1 = seq.substr(p, 6);
        std::string b2 = seq.substr((p + (6+2)), 6);
        std::string b3 = seq.substr((p + 2*(6+2)), 6);

        auto r1 = wl.find(b1);
        auto r2 = wl.find(b2);
        auto r3 = wl.find(b3);

        bool cond1 = r1 >= 0;
        bool cond2 = r2 >= 0;
        bool cond3 = r3 >= 0;
        std::string key = std::to_string(cond1) + std::to_string(cond2) + std::to_string(cond3);
        counts[key]++;

        key = "b1:" + soloreturns::getStateFromKey(r1);
        states[key]++;
        key = "b2:" + soloreturns::getStateFromKey(r2);
        states[key]++;
        key = "b3:" + soloreturns::getStateFromKey(r3);
        states[key]++;
    }
    if (r != -1) std::cerr << "errror: malformated FASTX" << std::endl;
	kseq_destroy(kseq);
	gzclose(fp);

    for (const auto& [key, count] : counts) {
        std::cout << key << ": " << count << " , " << 100.0*count/n << std::endl;
    }
    std::cout << "total reads: " << n << std::endl;

    for (const auto& [key, count] : states) {
        std::cout << key << ": " << count << " , " << 100.0*count/(3*n) << std::endl;
    }
    std::cout << "total barcodes: " << 3*n << std::endl;
    
    return 0;
}