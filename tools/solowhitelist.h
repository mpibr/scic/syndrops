#ifndef SOLOWHITELIST_H
#define SOLOWHITELIST_H
#include <iostream>
#include <fstream>
#include <string>
#include <unordered_set>

namespace soloreturns {
    constexpr int CONTAIN_N = -2;
    constexpr int MISMATCH = -1;
    constexpr int EXACTMATCH = 0;
    constexpr int ONEMISMATCH = 1;
    static std::string getStateFromKey(int key) {
        std::string state = "UNKNOWN";
        switch(key) {
            case CONTAIN_N:
                state = "CONTAIN_N";
                break;
            case MISMATCH:
                state = "MISMATCH";
                break;
            case EXACTMATCH:
                state = "EXACTMATCH";
                break;
            case ONEMISMATCH:
                state = "ONEMISMATCH";
                break;
            default:
                break;
        }
        return state;
    }
}


class solowhitelist {
public:
    explicit solowhitelist() = default;

    int load(const std::string &file) {
        std::ifstream fhin(file);
        if (!fhin.is_open()) {
            std::cerr << "error, opening file: " << file << std::endl;
            return -1; 
        }

        std::string kmer;
        while (std::getline(fhin, kmer)) {
            if (!kmer.empty()) {
                uint64_t code = 0;
                int index_ns = encodeDNAtoUInt(code, kmer);
                if (index_ns != 0)
                    continue; // skip barcodes with Ns
                whitelist_.insert(code);
            }
        }
        return 0;
    }


    int find(const std::string &barcode) {
        uint64_t barcode_key = 0;
        int index_ns = encodeDNAtoUInt(barcode_key, barcode);
        if (index_ns > 1) {
            return soloreturns::CONTAIN_N;
        }

        // check exact match
        if (auto iter = whitelist_.find(barcode_key); iter != whitelist_.end())
            return soloreturns::EXACTMATCH;
        
        // check 1MISMATCH
        // mutate each base in barcode and check for exact match
        for (int i = 0; i < barcode.length(); ++i) {
            for (int base = 1; base < 4; ++base) {
                uint64_t barcode_key_next = barcode_key^(base<<(i*2));
                if (auto iter = whitelist_.find(barcode_key_next); iter != whitelist_.end())
                    return soloreturns::ONEMISMATCH;
            }
        }

        return soloreturns::MISMATCH;
    }


    int size() {return whitelist_.size();}

private:
    static int encodeDNAtoUInt(uint64_t &encoded, const std::string &kmer) {
        int index_ns = 0; // non-standard bases
        encoded = 0; // reset code
        for (char base : kmer) {
            encoded <<= 2;
            switch (base) {
                case 'A': // 00 is already the result of shifting
                    break;
                case 'C':
                    encoded |= 1; // 01
                    break;
                case 'G':
                    encoded |= 2; // 10
                    break;
                case 'T':
                    encoded |= 3; // 11
                    break;
                default:
                    index_ns++;
            }
        }
        return index_ns;
    }

    std::unordered_set<uint64_t> whitelist_;
};

#endif // SOLOWHITELIST_H